"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from url_response import views
from django.conf.urls import url
from django.contrib import admin


from url_response .views import HomeView,  ChartData, getUrl, display, ResponseCalc
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home_view.as_view()),
    path('narad/', views.narad_view.as_view()),
    path('testing/', views.testing_view.as_view()),
    url(r'^testing/x/$', HomeView.as_view(), name='home'),
    path('chart/<int:url_id>/', ChartData.as_view()),
    url(r'^admin/', admin.site.urls),
    url(r'^testing/url/$',views.getUrl, name='testing'),
    url(r'^testing/response/$', display, name='display_testing'),
    url(r'^testing/start/$', ResponseCalc.as_view(), name="start_test"),
    path('url_detail/<int:url_id>/',  HomeView.as_view(), name='url_detail'),
    path('filter_response', views.filterResponse, name='filter_user'),
   ]

