from django.apps import AppConfig


class UrlResponseConfig(AppConfig):
    name = 'url_response'
