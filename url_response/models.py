from django.db import models
from datetime import datetime
# Create your models here.


class URL(models.Model):
    url = models.URLField(max_length=100)
    hit_frequency = models.IntegerField(null=True)
    hit_strength = models.IntegerField(null=True)
    #name, desc, active
    #Link=models.CharField(max_length=100)
    #Active=models.BooleanField()

class Transaction(models.Model):
    urls=models.ManyToManyField(URL)
    threshold=models.FloatField()
    description=models.CharField(max_length=100)

class Response_Times(models.Model):
    url = models.ForeignKey(URL, null=True, blank=True, related_name="response_times", on_delete=models.CASCADE)
    reponse_time = models.FloatField(default=0)

class Calculation(models.Model):
    url = models.ForeignKey(URL, null=True, blank=True, related_name="calculation", on_delete=models.CASCADE)
    timestamp = models.TimeField(default=datetime.now)
    min_reponse_time=models.FloatField(null=True)
    max_reponse_time = models.FloatField(null=True)
    mean_reponse_time = models.FloatField(null=True)
    median_reponse_time = models.FloatField(null=True)
    error=models.IntegerField(default=0)

