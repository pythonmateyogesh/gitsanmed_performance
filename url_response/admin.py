from django.contrib import admin
from url_response .models import URL, Transaction,Response_Times, Calculation
# Register your models here.
admin.site.register(URL)
admin.site.register(Response_Times)
admin.site.register(Calculation)
