from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth import get_user_model
from django.shortcuts import render
from django.views.generic import View
import urllib.request,urllib.parse
import threading,schedule,requests, time
from urllib.request import urlopen
from rest_framework.views import APIView
from rest_framework.response import Response
from statistics import mean, median
from datetime import datetime
from django.http import HttpResponse, JsonResponse
from django.core.files.storage import FileSystemStorage
from django.core import serializers
import datetime, os, json
import requests
import json
from url_response .models import URL, Response_Times, Calculation

class home_view(TemplateView):
    template_name = "index.html"


class narad_view(TemplateView):
    template_name = "narad.html"


class base_view(TemplateView):
    template_name = "base.html"

class testing_view(TemplateView):
    template_name = "testing.html"

def getUrl(request):
    if request.method=='POST':
        if request.POST.get('url'):
            if request.POST.get('hit_frequency'):
                if request.POST.get('hit_strength'):

                    post=URL()
                    post.url=request.POST.get('url')
                    post.hit_frequency = request.POST.get('hit_frequency')
                    post.hit_strength=request.POST.get('hit_strength')
                    post.save()
                    return render(request,'testing.html')
    else:
        return render(request, 'testing.html')

class HomeView(View):
    def get(self, request,url_id,  *args, **kwargs):
        url = URL.objects.get(id=url_id)
        return render(request, 'charts.html', {"url":url,"ResponseTime": 0.94})

def display(request):
    info=URL.objects.all()
    return render(request,"response.html",{'urls':info})

class URLDetail(APIView):
    def get(self, request, url_id, format=None):
        info = URL.objects.get(id=url_id)
        print("info",url_id)
        return render(request,"response.html",{'urls':info})

class ResponseCalc(APIView):
    def get(self,request, format=None):
       #schedule.every().day.at("15:00").do()
        self.run()
        return Response()

    def run(self):
        for url in URL.objects.all():
            for hit_frequency in URL.objects.all():
                for hit_strength in URL.objects.all():
                    url=url.url
                    hit_frequency= hit_frequency.hit_frequency
                    hit_strength = hit_strength.hit_strength
                    self.schedulee(url,hit_strength,hit_frequency)

    def getr(self,url):
            try:
                print(url)
                r = requests.get(url, timeout=10)
                r.raise_for_status()
                respTime = (round(r.elapsed.total_seconds(), 2))
                try:
                    url_obj = URL.objects.get(url=url)
                except:
                    url_obj = URL.objects.create(url=url)
                resp = Response_Times()
                resp.url=url_obj
                resp.reponse_time=respTime
                resp.save()

            except requests.exceptions.HTTPError as err01:
                print("HTTP error: ", err01)
            except requests.exceptions.ConnectionError as err02:
                print("Error connecting: ", err02)
            except requests.exceptions.Timeout as err03:
                print("Timeout error:", err03)
            except requests.exceptions.RequestException as err04:
                print("Error: ", err04)

    def threadings(self,url,hit_strength):
        threads = []
        for i in range(hit_strength):
            t = threading.Thread(target=self.getr(url))
            threads.append(t)
            t.start()
        self.getresponse(url)

    def getresponse(self, url):
        for url in URL.objects.all():
            url = URL.objects.get(url=url)
            response_time_queryset = Response_Times.objects.filter(url = url)
            response_time_list = []
            for response_time in response_time_queryset:
                if response_time.reponse_time != 0:
                    response_time_list.append(response_time.reponse_time)
            minresponse = min(response_time_list)
            maxresponse = max(response_time_list)
            meanresponse = mean(response_time_list)
            medianresponse = median(response_time_list)
            respt = Calculation()
            respt.url = url
            respt.min_reponse_time = minresponse
            respt.max_reponse_time = maxresponse
            respt.mean_reponse_time = meanresponse
            respt.median_reponse_time = medianresponse
            respt.save()
            #Response_Times.objects.delete()

    def schedulee(self,url,hit_strength,hit_frequency):
        start = time.time()
        PERIOD_OF_TIME = 180
        schedule.every(hit_frequency).minutes.do(self.threadings,url,hit_strength)
        while True:
            schedule.run_pending()
            time.sleep(1)
            if time.time() > start + PERIOD_OF_TIME: break

def filterResponse(request):
    all_calc = Calculation.objects.all()
    print(all_calc)
    return Response

class ChartData(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, url_id, format=None):
        response_time_queryset = Calculation.objects.filter(url__id=url_id)
        requeired_response_time_list = []
        labels = []
        print('request.GET["target"]')
        for response_time in response_time_queryset:
            labels.append(response_time.timestamp)
            if request.GET["target"] == "min_response_time":
                requeired_response_time_list.append(response_time.min_reponse_time)
            elif request.GET["target"] == "max_response_time":
                requeired_response_time_list.append(response_time.max_reponse_time)
            elif request.GET["target"] == "mean_response_time":
                requeired_response_time_list.append(response_time.mean_reponse_time)
            elif request.GET["target"] == "median_response_time":
                requeired_response_time_list.append(response_time.median_reponse_time)
            elif request.GET["target"] == "error":
                requeired_response_time_list.append(response_time.error)
        print("default_items",requeired_response_time_list)
        print("labels",labels)
        data = {
                "labels": labels,
                "default": requeired_response_time_list,
        }

        return Response(data)

# class ChartData(APIView):
#     authentication_classes = []
#     permission_classes = []
#     def get(self, request, url_id, format=None):
#         response_time_queryset = Calculation.objects.filter(url__id=url_id)
#         response_time_list = []
#         for min_response_time in response_time_queryset:
#             response_time_list.append(min_response_time.min_response_time)
#         labels = ["minresponse","maxresponse","meanresponse","medianresponse"]
#         default_items = [minresponse,maxresponse,meanresponse,medianresponse]
#         print("default_items",default_items)
#         data = {
#                 "labels": labels,
#                 "default": default_items,
#         }
#         return Response(data)
